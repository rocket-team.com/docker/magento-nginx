FROM nginx

MAINTAINER Oleksii Stepanov <sagerbox@gmail.com>

COPY magento.conf  /etc/nginx/conf.d/default.conf
